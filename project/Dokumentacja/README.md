# OSP-project (Czytnik/generator kodów typu barcode)

## Prowadzący:
 - Dr hab. inż. Sebastian Budzan, prof. Pol.Śl.

## Projekt nr. 9:
Kategoria: Wizja maszynowa/LabVIEW/Adaptive Vision.
Cel: aplikacja powinna umożliwiać detekcję kodów minimum 3 rodzajów. Obraz powinien być
rejestrowany bezpośrednio z kamery. W zakresie detekcji aplikacja powinna umożliwiać rozróżnienie
3 typów kodów oraz rozpoznawać jeden z nich, np. EAN-13. Przy sekcjach 4/5 osobowych dodatkowo
generowanie kodów EAN-13 oraz zapis do bazy (MS SQL Server Express) wyników rozpoznawania.

## Autorzy:
 - Patryk Kucia
 - Daniek Jońca
 - Rafał Szymura
 - Piotr Dinges


## HARMONOGRAM

| Data  | Czynność |
| ------------- | ------------- |
| 16.03  |Przygotowanie harmonogramiu, założenie repozytorium, podział prac,szablon projektu JKI |
| 30.04  |Implementacja podstawowych funkcji, Odczyt,zapisywanie Barcode |
| 14.05  |Projekt interfejsu użytkownika, stworzenie modułu generującego kody Barcode |
| 28.05  |Adaptacja bazy danych |
| 11.06  |Testy poprawa błędów i optymalizacja |
| 18.06  |Prezentacja projektu |

## 1 i 2 Tydzień
1. Utworzenie czystego projektu.
2. Dodanie szablonu JKI State Machine i stanu Action: Initialize i Image processing:
![stany](image-6.png)

3. Pobranie Vision Acquisition Software. ([strona](https://www.ni.com/en/support/downloads/drivers/download.vision-acquisition-software.html#477251))
![instalacja](image.png)

4. Zapoznanie się z funkcjami Vision Acquisition
![menu_rozwijane](image-1.png)

5. Do poprawnego działania kamey niezbędne było ustawienie odpowiednich ustawień dotyczących używanej przez nas kamery w NI MAX:
![ustawienia_kamery](image-2.png)

6. Stworzenie prostego projektu dekodującego Barcode 1D ([tutorial](https://www.youtube.com/watch?v=B3kif7JfT7g)).
![simple_barcode_detector](image-3.png)
![screen_camera1](image-4.png)

7. Dodanie najprostszego wyświetlania błędów
![error1](image-5.png)



## 3 i 4 Tydzień
1. Uporządkowanie Bookmarks
2. Wybranie typów wyszukiwanych przez nas kodów (sugerowaliśmy się różną liczbą zaspisywanych cyfr [kody](https://uk.onlinelabels.com/articles/introduction-to-barcodes)):
- [EAN-13](https://pl.wikipedia.org/wiki/EAN)
![ean13](image-13.png)
- [EAN-8](https://pl.wikipedia.org/wiki/EAN)
![ean8](image-14.png)
- [Code 128](https://pl.wikipedia.org/wiki/Kod_128)
![code128](image-12.png)

Zestaw testowy:
![testCodes](image-15.png)
Własności:
![dl_ean](image-16.png)


2. Dodanie stnu Save Settings
![save_settings](image-7.png)

3. Modyfikacja dekodera Barcode - ramka wokół kodu i numer kodu.
![ramka_tekst](image-8.png)
![kod_wykryty](image-9.png)

4, Dodanie wykrywania drugiego barecodu (kolejnośc wykrywania jest nieprzewidywalna)
![dwa_kody](image-10.png)
![kod_na2barcody](image-11.png)
Trzy kody:
![trzy_kody](image-18.png)
![detekcja3](image-20.png)

4. Dodanie bazy danych **MS SQL Server Express.**


